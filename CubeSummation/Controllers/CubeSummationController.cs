﻿using CubeSummation.BussinesLogic;
using CubeSummation.Models;
using System;
using System.Web.Mvc;

namespace CubeSummation.Controllers
{

    public class CubeSummationController : Controller
    {
        CubeSummationProcess bussinesLogicCube = new CubeSummationProcess();
        long resultProcess = 0;

        // GET: CubeSummation
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult AjaxUpdate(CubeSummationModel cubeModelData)
        {
            CubeModel cubeModel = new CubeModel
            {
                x = cubeModelData.x,
                y = cubeModelData.y,
                z = cubeModelData.z,
                value = cubeModelData.value,
                section = cubeModelData.section
            };

            resultProcess = bussinesLogicCube.Process(cubeModelData.Dimension, cubeModelData.NOperacion, cubeModelData.TOperacion, cubeModel);
            return Json(resultProcess, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AjaxSearch(CubeSummationModel cubeModelData)
        {
            CubeModel cubeModel = new CubeModel
            {
                x = cubeModelData.x,
                y = cubeModelData.y,
                z = cubeModelData.z,
                x0 = cubeModelData.x0,
                y0 = cubeModelData.y0,
                z0 = cubeModelData.z0,
                section = cubeModelData.section
            };

            resultProcess = bussinesLogicCube.Process(cubeModelData.Dimension, cubeModelData.NOperacion, cubeModelData.TOperacion, cubeModel);
            return Json(resultProcess, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AjaxCleanMatrix(int section)
        {
            resultProcess = 0;
            if (bussinesLogicCube.CleanSectionMatriz(section)) resultProcess = 1;
            return Json(resultProcess, JsonRequestBehavior.AllowGet);
        }

        private ActionResult agregar()
        {
            return View(2);
        }
    }
}