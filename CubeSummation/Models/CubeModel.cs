﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CubeSummation.Models
{
    public class CubeModel
    {
        public CubeModel() { }

        public long x0 { get; set; }
        public long y0 { get; set; }
        public long z0 { get; set; }
        public long x { get; set; }
        public long y { get; set; }
        public long z { get; set; }
        public long value { get; set; }
        public int section { get; set; }

    }

}