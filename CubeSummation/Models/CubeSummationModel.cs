﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CubeSummation.Models
{
    public class CubeSummationModel
    {

        [Display(Name = "Casos de prueba")]
      ///  [Required(ErrorMessage = "El campo Aoc es requerido")]
        public int Cprueba { get; set; } 

        [Display(Name = "Dimension Matriz")]
     ///[Required(ErrorMessage = "El campo Nombre es requerido")]
        public int Dimension { get; set; }

        [Display(Name = "# Operaciones")]
        public int NOperacion { get; set; }

        [Display(Name = "Tipo de operacion")]
        public string TOperacion { get; set; }

        [Display(Name = "Coordenada x")]
        public int x  { get; set; }

        [Display(Name = "Coordenada y")]
        public int y { get; set; }

        [Display(Name = "Coordenada z")]
        public int z { get; set; }

        [Display(Name = "Coordenada x0")]
        public int x0 { get; set; }

        [Display(Name = "Coordenada y0")]
        public int y0 { get; set; }

        [Display(Name = "Coordenada z0")]
        public int z0 { get; set; }

        [Display(Name = "value")]
        public long value { get; set; }

        [Display(Name = "section")]
        public int section { get; set; }
    }
}