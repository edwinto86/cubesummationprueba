$(document).ready(function () {
    document.getElementById("alertError").style.display = "none";

    //Set Global TestCases
    var Cases = $('#Cprueba').val();

    //interation with CasosPruebas
    $('#Cprueba').change(function () {
        Cases = $('#Cprueba').val();
        var num = 0;
        var div = "";
        if (Cases > 0 && Cases < 6) {
            for (var i = 0; i < Cases; i++) {
                num = (i + 1);
                div += BuildTestCase(num);
            }
        }
        document.getElementById("Result").innerHTML = div;
    });
});

//Custom Operation when select the Event
function CustomOperation(value) {
    var operationSelected = "#operation" + value;
    var sectionOperation = "sectionOperation" + value;
    var operationValue = $(operationSelected).val();
    var div = "";

    for (var i = 0; i < operationValue; i++) {
        operation = (i + 1);
        div += BuildOperations(value, operation);
    }
    ClearMatrix(value);
    document.getElementById(sectionOperation).innerHTML = div;
}

//BuildHtml TestCases
function BuildTestCase(num) {
    return '<div class="content-general">'
        + '<div class="title"> Caso de Prueba ' + num + '</div>'
        + '<div class="model-space">Matriz = <input class="wt50" id="matriz' + num + '" type=number data-val-required="obligatorio"/></div>'
        + '<div class="model-space">Operaciones = <input class="wt50" id="operation' + num + '" type=number data-val-required="obligatorio" onchange="CustomOperation(' + num + ');"/></div>'
        + '<div id="sectionOperation' + num + '"></div>'
        + '</div>';
}

//Build Div Operations
function BuildOperations(section, numOperation) {
    var num = section.toString() + numOperation.toString();
    return '<div class="operationSection">'
        + '<div class="model-space">Consulta'
        + '<select id="typeOperation' + num + '" name="typeOperation' + num + '" onchange="CustomTypeOperation(' + section + ',' + numOperation + ');">'
        + '<option value="" selected>Sele..</option>'
        + '<option value="UPDATE">UPDATE</option>'
        + '<option value="QUERY">QUERY</option>'
        + '</select >'
        + '<div id="sectionTypeOperations' + num + '"></div>'
        + '</div>'
        + '</div>';
}

//Custom TypeOperation when select the Event
function CustomTypeOperation(section, numOperation) {
    var value = section.toString() + numOperation.toString();
    var fieldSelect = $('select[name=typeOperation' + value + ']').val();
    var div = "";
    var result = "sectionTypeOperations" + value;

    if (fieldSelect !== "") {
        if (fieldSelect === "UPDATE") {
            div = '<div>Coordenada x = <input class="wt50 model-space" id="coordenadax' + value + '" type=number /></div>'
                + '<div>Coordenada y = <input class="wt50 model-space" id="coordenaday' + value + '" type=number /></div>'
                + '<div>Coordenada z = <input class="wt50 model-space" id="coordenadaz' + value + '" type=number /></div>'
                + '<div>Valor = <input class="wt50 model-space" id="valor' + value + '" type=number /></div>'
                + '<input type="button" value="Actualizar" class="btn btn-primary model-space" onclick="Update(' + section + ',' + numOperation + ');"/>'
                + '<div class="alert alert-light model-space" role="alert" id="alertSuccessUpdate' + value + '" ></div >';
            document.getElementById(result).innerHTML = div;
        } else {
            div = '<div>Coordenada X0 = <input class="wt50 model-space" id="coordenadax0' + value + '" type=number /></div>'
                + '<div>Coordenada y0 = <input class="wt50 model-space" id="coordenaday0' + value + '" type=number /></div>'
                + '<div>Coordenada z0 = <input class="wt50 model-space" id="coordenadaz0' + value + '" type=number /></div>'
                + '<div>Coordenada x = <input class="wt50 model-space" id="coordenadaQx' + value + '" type=number /></div>'
                + '<div>Coordenada y = <input class="wt50 model-space" id="coordenadaQy' + value + '" type=number /></div>'
                + '<div>Coordenada z = <input class="wt50 model-space" id="coordenadaQz' + value + '" type=number /></div>'
                + '<input type="button" value="Consultar" class="btn btn-warning model-space" onclick="Search(' + section + ',' + numOperation + ');"/>'
                + '<div class="alert alert-light model-space" role="alert" id="alertSuccessSearch' + value + '" ></div >';;
            document.getElementById(result).innerHTML = div;
        }
    }
}

//Update Values
function Update(section, numOperation) {
    var CubeModels = new Object();
    var value = section.toString() + numOperation.toString();
    var pathcontroller = "/cubesummation/AjaxUpdate";

    CubeModels.Dimension = $("#matriz" + section).val();
    CubeModels.NOperacion = $("#operation" + section).val();
    CubeModels.TOperacion = $('select[name=typeOperation' + value + ']').val();
    CubeModels.x = $("#coordenadax" + value).val();
    CubeModels.y = $("#coordenaday" + value).val();
    CubeModels.z = $("#coordenadaz" + value).val();
    CubeModels.value = $("#valor" + value).val();
    CubeModels.section = section;
    CallMethodAjax(CubeModels, pathcontroller, 1, value);
}

//Search Values
function Search(section, numOperation) {
    var CubeModels = new Object();
    var value = section.toString() + numOperation.toString();
    var pathcontroller = "/cubesummation/AjaxSearch";

    CubeModels.Dimension = $("#matriz" + section).val();
    CubeModels.NOperacion = $("#operation" + section).val();
    CubeModels.TOperacion = $('select[name=typeOperation' + value + ']').val();
    CubeModels.section = section;
    CubeModels.x0 = $("#coordenadax0" + value).val();
    CubeModels.y0 = $("#coordenaday0" + value).val();
    CubeModels.z0 = $("#coordenadaz0" + value).val();
    CubeModels.x = $("#coordenadaQx" + value).val();
    CubeModels.y = $("#coordenadaQy" + value).val();
    CubeModels.z = $("#coordenadaQz" + value).val();

    CallMethodAjax(CubeModels, pathcontroller, 2, value);
}

//Delete Matrix by section call controller
function ClearMatrix(section) {
    var CubeModels = new Object();
    var pathController = "/cubesummation/AjaxCleanMatrix";
    CubeModels.section = section;

    $.ajax({
        type: "POST",
        url: pathController,
        data: JSON.stringify(CubeModels),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            if (response == null) {
                document.getElementById("alertError").style.display = "block";
                document.getElementById("alertError").innerHTML = "Error En " + pathController;
            }
        },
        failure: function (response) {
            console.log(response);
            document.getElementById("alertError").style.display = "block";
            document.getElementById("alertError").innerHTML = "Failure in function ClearMatrix";
        },
        error: function (response) {
            console.log(response);
            document.getElementById("alertError").style.display = "block";
            document.getElementById("alertError").innerHTML = "Error in function ClearMatrix";
        }
    });
}

//Call controller by Ajax
function CallMethodAjax(CubeModels, pathController, op, value) {
    $.ajax({
        type: "POST",
        url: pathController,
        data: JSON.stringify(CubeModels),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            if (response != null) {

                if (op == 1 && response == 1) {
                    document.getElementById("alertSuccessUpdate" + value).innerHTML = "Registro Actualizado Correctamente";
                } else if (op == 2) {
                    document.getElementById("alertSuccessSearch" + value).innerHTML = "Resultado = " + response;
                } else {
                    console.log(response);
                    document.getElementById("alertError").style.display = "block";
                    document.getElementById("alertError").innerHTML = "Error En " + pathController;
                }
            } else {
                console.log(response);
                document.getElementById("alertError").style.display = "block";
                document.getElementById("alertError").innerHTML = "Error En " + pathController;
            }
        },
        failure: function (response) {
            console.log(response);
            document.getElementById("alertError").style.display = "block";
            document.getElementById("alertError").innerHTML = "Failure in function ClearMatrix";
        },
        error: function (response) {
            console.log(response);
            document.getElementById("alertError").style.display = "block";
            document.getElementById("alertError").innerHTML = "Error in function ClearMatrix";
        }
    });
}
