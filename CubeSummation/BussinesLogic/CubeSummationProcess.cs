﻿using CubeSummation.Models;
using System;

namespace CubeSummation.BussinesLogic
{
    public class CubeSummationProcess : MatrixFileProcess
    {

        long[,,] Matrix = new long[101, 101, 101];
        MatrizModel matrixLoad = new MatrizModel();


        /// <summary>
        /// Proceso principal
        /// </summary>
        /// <param name="n"> dimension matriz </param>
        /// <param name="m"> numero de oeraciones a ejecutar</param>
        public long Process(long n, long m, string op, CubeModel cubeModel)
        {
            try
            {
                long x, y, z, x0, y0, z0;
                long value1, value2, val, resultM = 0;

                if (op.Equals("QUERY"))
                {

                    //Load Matrix
                    matrixLoad = LoadMatrixFile(cubeModel.section);
                    Matrix = matrixLoad.Matrix;

                    //cordenada x0
                    x0 = cubeModel.x0;
                    //cordenada y0
                    y0 = cubeModel.y0;
                    //cordenada z0
                    z0 = cubeModel.z0;
                    //cordenada x
                    x = cubeModel.x;
                    //cordenada y
                    y = cubeModel.y;
                    //cordenada z
                    z = cubeModel.y;

                    value1 = Calculate_sum(x, y, z) - Calculate_sum(x0 - 1, y, z)
                            - Calculate_sum(x, y0 - 1, z) + Calculate_sum(x0 - 1, y0 - 1, z);

                    value2 = Calculate_sum(x, y, z0 - 1) - Calculate_sum(x0 - 1, y, z0 - 1)
                            - Calculate_sum(x, y0 - 1, z0 - 1) + Calculate_sum(x0 - 1, y0 - 1, z0 - 1);

                    resultM = value1 - value2;
                }

                if (op.Equals("UPDATE"))
                {
                    //Load Matrix
                    matrixLoad = LoadMatrixFile(cubeModel.section);
                    if (matrixLoad.Matrix != null)
                    {
                        Matrix = matrixLoad.Matrix;
                    }


                    //cordenada x
                    x = cubeModel.x;
                    //cordenada y
                    y = cubeModel.y;
                    //cordenada z
                    z = cubeModel.z;
                    //valor
                    val = cubeModel.value;
                    x0 = x;
                    y0 = y;
                    z0 = z;

                    value1 = Calculate_sum(x, y, z) - Calculate_sum(x0 - 1, y, z)
                            - Calculate_sum(x, y0 - 1, z) + Calculate_sum(x0 - 1, y0 - 1, z);
                    value2 = Calculate_sum(x, y, z0 - 1) - Calculate_sum(x0 - 1, y, z0 - 1)
                            - Calculate_sum(x, y0 - 1, z0 - 1) + Calculate_sum(x0 - 1, y0 - 1, z0 - 1);

                    Update(n, x, y, z, val - (value1 - value2), cubeModel.section);
                    resultM = 1;
                }
                return resultM;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para limpiar matriz del proceso
        /// </summary>
        /// <param name="section">sección o caso de pruebas</param>
        /// <returns></returns>
        public bool CleanSectionMatriz(int section) {
            try
            {
                return CleanMatrixBySection(section);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// metodo de operacion actualizar
        /// </summary>
        /// <param name="n"> matriz </param>
        /// <param name="x"> coordena</param>
        /// <param name="y"> coordenada</param>
        /// <param name="z"> cordena </param>
        /// <param name="val"> valor actualizar</param>
        private void Update(long n, long x, long y, long z, long val, int section)
        {
            long y1, x1;

            while (z <= n)
            {
                x1 = x;
                while (x1 <= n)
                {
                    y1 = y;
                    while (y1 <= n)
                    {
                        Matrix[x1, y1, z] += val;
                        y1 += (y1 & -y1);
                    }
                    x1 += (x1 & -x1);
                }
                z += (z & -z);
            }

            SaveMatrixFile(Matrix, section);

        }

        /// <summary>
        /// metodo calcula suma|
        /// </summary>
        /// <param name="x">coordenadas</param>
        /// <param name="y">coordenadas</param>
        /// <param name="z">coordenadas</param>
        /// <returns></returns>
        private long Calculate_sum(long x, long y, long z)
        {
            long y1, x1, sum = 0;
            while (z > 0)
            {
                x1 = x;
                while (x1 > 0)
                {
                    y1 = y;
                    while (y1 > 0)
                    {
                        sum += Matrix[x1, y1, z];
                        y1 -= (y1 & -y1);
                    }
                    x1 -= (x1 & -x1);
                }
                z -= (z & -z);

            }
            return sum;
        }
    }
}

