﻿using CubeSummation.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;

namespace CubeSummation.BussinesLogic
{
    public class MatrixFileProcess
    {
        MatrizModel matrixLoad = new MatrizModel();
        string jsonFileMatrix = ConfigurationManager.AppSettings["JsonFileMatrixPersistence"];
        string filename = "file";
        string format = ".json";

        protected void SaveMatrixFile(long[,,] Matrix, int section)
        {
            try
            {
                ValidateDirectory();
                // write the data
                matrixLoad.Matrix = Matrix;
                string PathAllFileMatrix = jsonFileMatrix + filename + section + format; ;
                using (var stream = new StreamWriter(PathAllFileMatrix, append: false))
                {
                    stream.Write(JsonConvert.SerializeObject(matrixLoad));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected MatrizModel LoadMatrixFile(int section)
        {
            try
            {
                ValidateDirectory();
                string PathAllFileMatrix = jsonFileMatrix + filename + section + format;
                //read the data

                if (ValidateFileExists(PathAllFileMatrix))
                {
                    using (var stream = new StreamReader(PathAllFileMatrix))
                    {
                        matrixLoad = JsonConvert.DeserializeObject<MatrizModel>(stream.ReadToEnd());
                    }
                }
                return matrixLoad;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        protected bool CleanMatrixBySection(int section)
        {
            ValidateDirectory();
            string PathAllFileMatrix = jsonFileMatrix + filename + section + format;

            if (ValidateFileExists(PathAllFileMatrix))
            {
                File.Delete(PathAllFileMatrix);
                return true;
            }
            else
            {
                return false;
            }
        }

        private void ValidateDirectory()
        {
            if (!Directory.Exists(jsonFileMatrix))
            {
                Directory.CreateDirectory(jsonFileMatrix);
            }
        }
        private bool ValidateFileExists(string fullPath)
        {
            if (File.Exists(fullPath))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}