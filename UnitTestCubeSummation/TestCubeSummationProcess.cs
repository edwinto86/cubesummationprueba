﻿using CubeSummation.BussinesLogic;
using CubeSummation.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Configuration;

namespace UnitTestCubeSummation
{
    [TestClass]
    public class TestCubeSummationProcess
    {
        CubeSummationProcess CubeSummationProcess = new CubeSummationProcess();

        
        [TestMethod]
       
        public void ValidateProcessUpdateAndSearch()
        {
            CubeModel cubeModel = new CubeModel();
            long n; long m; string op;

            string value = ConfigurationManager.AppSettings["JsonFileMatrixPersistence"];
            Assert.IsNotNull(value, "No App.Config found.");

            //SECTION FOR UPDATE
            n = 2;
            m = 2;
            op = "UPDATE";
            cubeModel.x = 1;
            cubeModel.y = 1;
            cubeModel.z = 1;
            cubeModel.value = 2;
            var resultUpdate = CubeSummationProcess.Process(n, m, op, cubeModel);

            if (resultUpdate == 1)
            {
                //SECTION FOR QUERY
                op = "QUERY";
                cubeModel.x = 2;
                cubeModel.y = 2;
                cubeModel.z = 2;
                cubeModel.x0 = 1;
                cubeModel.y0 = 1;
                cubeModel.z0 = 1;
                var resultSearch = CubeSummationProcess.Process(n, m, op, cubeModel);
                Assert.AreEqual(cubeModel.value, resultSearch);
            }
            else
            {
                Assert.AreEqual(resultUpdate, 1);
            }
        }
    }
}
