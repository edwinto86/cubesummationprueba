# Cube Summation Model
A continuación, se describe la información de la solución, de igual manera se indica los pasos a realizar para la respectiva configuración de la misma:

### 1. Configuración Proyecto
Vamos al __Web.Config__ de la solución y realizamos la configuración de la ruta donde debe persistir nuestra matriz, para esto almacenamos la ruta en la llave con nombre __JsonFileMatrixPersistence__, por ejemplo: __value="C:/TestLab/"__

### 2. Configurar Pruebas Unitarias
Para la configuración de las pruebas unitarias se realiza el mismo procedimiento anterior, pero sobre el archivo __App.conf__ de la solución Test.

### 3. Correr el proyecto
Para observar el proyecto abrimos la solución en Visual Studio 2019 y observamos dos capas una web(__CubeSummation__) y otra de pruebas(__UnitTestCubeSummation__).

#
# Solución Preguntas

- Las capas de la aplicación (por ejemplo, capa de persistencia, vista, de aplicación, etc) y qué clases pertenecen a cuál.

__Rpta:__ Para la solución del requerimiento se utilizó el patrón de arquitectura __MVC__, definiendo el modelo, la vista y el controlador en el proyecto __CubeSummation__, de igual manera se utilizaron clases con restricciones y niveles de seguridad básicos en el ámbito de desarrollo de software; a continuación un poco de detalle de las capas:

- __BussinesLogic:__ Utilizada como capa de persistencia de datos, en este espacio encontraremos las clases con responsabilidad única.
- __Controllers:__ Controladores de las vistas, Utilizada como capa de negocio.
- __Models:__ Modelos públicos usados como públicos para todas sus capas sin ninguna restricción.
- __Views y Scripts:__ Componentes encargados de la capa de presentación de la aplicación

__a. ¿En qué consiste el principio de responsabilidad única?__

Este principio aunque se usa muy poco por temas de madurez, capacitación y exigencia en los equipos de trabajo de desarrollo de una organización, hace referencia a que una clase o módulo debe tener una única responsabilidad, con el fin de implementar en cada uno de ellas lo necesario, es decir, mantener ‘unidas’ funcionalidades que estén relacionadas entre sí y mantener fuera aquello que no esté relacionado.

Una ventaja que nos da este principio es el bajo acoplamiento, obteniendo muchos beneficios como mantenibilidad, extensibilidad, testeable y entre otros. El objetivo es disminuir el acoplamiento entre aquellas cosas que cambian de forma diferente.

__b. ¿Qué es código limpio?__
Este principio lo que busca es que el código tenga una estructura legible y entendible, pensando en el futuro, para aquellas personas que quizás necesiten modificar una o varias líneas y lo pueda hacer de manera muy rápida; sin lugar a duda, esto está atado a las buenas prácticas de desarrollo de software que busca siempre una codificación limpia en el sentido de que se utilicen variables entendibles, métodos entendibles y una buena estructura.
